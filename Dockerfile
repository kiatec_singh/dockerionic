### STAGE 1: Build ###
# stage 1
FROM node:latest as node
COPY package.json package-lock.json ./
RUN npm ci && mkdir /dockerionic && mv ./node_modules ./dockerionic

# WORKDIR /app
COPY . .
RUN npm install 
RUN npm install -g ionic@3.9.2
# RUN npm install --save rebuild-node-sass node-sass 
EXPOSE 8100 35729

# Step2
WORKDIR /
RUN npm install
RUN echo '{"allow_root":true}' > /root/.bowerrc
RUN npm install -g ionic@3.9.2
# CMD ["nginx", "-g", "daemon off;"]
ENTRYPOINT ["ionic"]
CMD ["serve","--all"]
# , "--all", "--port", "8100", "--livereload-port", "35729"

